#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>
int m, s;

#define errExit(msg)                                                           \
  do {                                                                         \
    perror(msg);                                                               \
    exit(EXIT_FAILURE);                                                        \
  } while (0)

static int /* Start function for cloned child */
childFunc1(void *arg) {

  while (1) {
    sleep(60);
    m++;
  }
  /*write(1, "hello2\n", 7);*/

  childFunc1(NULL);
  return 0;
}
static int /* Start function for cloned child */
childFunc2(void *arg) {

  while (1) {
    /*system("clear");*/
    if (s % 60 == 0)
      s = 0;
    /*printf("%d : %d\n", m, (s++) % 60);*/
    sleep(1);
    s++;
    /*write(1, "hello1\n", 7);*/
  }
  /*write(1, "hello1\n", 7);*/

  /*childFunc2(NULL);*/

  return 0;
}

#define STACK_SIZE (1024 * 1024 * 1024) /* Stack size for cloned child */

int main(int argc, char *argv[]) {
  char *stack[2];    /* Start of stack buffer */
  char *stackTop[2]; /* End of stack buffer */
  pid_t pid[2];

  /* Allocate memory to be used for the stack of the child */

  stack[0] = mmap(NULL, STACK_SIZE, PROT_READ | PROT_WRITE,
                  MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
  stack[1] = mmap(NULL, STACK_SIZE, PROT_READ | PROT_WRITE,
                  MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
  if (stack[0] == MAP_FAILED)
    errExit("mmap");

  if (stack[1] == MAP_FAILED)
    errExit("mmap");
  stackTop[0] = stack[0] + STACK_SIZE; /* Assume stack grows downward */
  stackTop[1] = stack[1] + STACK_SIZE; /* Assume stack grows downward */

  /* Create child that has its own UTS namespace;
     child commences execution in childFunc() */

  pid[0] = clone(childFunc1, stackTop[0],
                 CLONE_THREAD | CLONE_SIGHAND | CLONE_VM | CLONE_IO |
                     CLONE_FILES | CLONE_FS | SIGCLD,
                 NULL);
  pid[1] = clone(childFunc2, stackTop[1],
                 CLONE_THREAD | CLONE_SIGHAND | CLONE_VM | CLONE_IO |
                     CLONE_FILES | CLONE_FS | SIGCLD,
                 NULL);
  while (1) {
    system("clear");
    printf("%d : %d\n", m, s);
    sleep(1);
  }
  /*sleep(200);*/

  /*if (waitpid(pid[0], NULL, 0) == -1) [> Wait for child <]*/
  /*errExit("waitpid");*/
  /*if (waitpid(pid[1], NULL, 0) == -1) [> Wait for child <]*/
  /*errExit("waitpid");*/

  if (pid[0] == -1)
    errExit("clone");
  if (pid[1] == -1)
    errExit("clone");

  printf("terminated\n");

  /* Parent falls through to here */
}
