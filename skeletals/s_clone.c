// reference: https://www.youtube.com/watch?v=1NXECbHd8mA&ab_channel=ShellWave
#define _GNU_SOURCE // this should be on top
#include <linux/sched.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define SIZESTACK (1024 * 1024)

static int child_proc(void *arg) {
  while (1)
    printf("hi\n");
  printf("Child process arg=%s\n", (char *)arg);
  sleep(4);
  return 0;
}

int main(int argc, char **argv) {
  char *stack, *stackname;
  char *str = "Hello world\n";
  pid_t parent_tid;
  pid_t pid = -1;
  int status;
  int storage;

  stack = (char *)malloc(SIZESTACK);
  if (!stack) {
    fprintf(stderr, "Unable to allocate stack\n");
    exit(1);
  }
  stackname = stack + SIZESTACK - 1;
  printf("Parent pid = %d\n", getpid());
  // pid = clone(child_proc,stackname,SIGCHLD,str);
  pid = clone(child_proc, stackname,
              CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_THREAD |
                  CLONE_SETTLS | CLONE_PARENT_SETTID | CLONE_CHILD_CLEARTID |
                  SIGCHLD,
              str, &parent_tid, &storage);
  // the code was breaking before since certain flags like clone_parent_setid
  // and clone_settls require additional variables as arguments!
  if (pid == -1) {
    fprintf(stderr, "Unable to clone");
    free(stack);
    exit(1);
  }
  // reference -
  // https://stackoverflow.com/questions/11295298/waiting-for-threads-of-another-process-using-waitpid

  waitpid(pid, &status, __WALL);

  while (1) {
    waitpid(pid, &status, __WALL);
    if (WIFEXITED(status)) // assume it will exit at some point
      break;
  }

  printf("Child pid = %d\n", parent_tid);
  printf("Done!\n");
  /*sleep(3);*/
  return 0;
}
