#include<stdio.h>
#include"vsthread.h"
typedef long long ll;

ll c,c1,c2;
pthread_spinlock spin;
enum Test1 {SPIN, MUTEX};
enum Test2 {M21, O1};
int test1 = SPIN;
int test2 = M21;

void* inc1(){
    while (c < 100000){
        pthread_spin_lock(&spin);
        // printf("thread 1 got the lock\n");
        c++;
        pthread_spin_unlock(&spin);
        c1++;
    }
    // printf("done1\n");

}
void* inc2(){
    // printf("I am here\n");
    while (c < 100000){
        pthread_spin_lock(&spin);
        // printf("thread 2 got the lock\n");
        c++;
        pthread_spin_unlock(&spin);
        c2++;
    }
    // printf("done\n");
}

int main(int argc, char **argv){

    pthread_t t1,t2;
    pthread_init(atoi(argv[1]));
    // pthread_spin_init(&lock);
    pthread_spin_init(&spin);
    pthread_create(&t1,NULL,inc1,NULL);
    pthread_create(&t2,NULL,inc2,NULL);
    pthread_join(t1,NULL);
    pthread_join(t2,NULL);
    printf("%lli\n%lli\n",c,c1+c2);
    return 0;
}
