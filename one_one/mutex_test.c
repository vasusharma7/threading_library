/**
 * @file mutex_test.c
 * @author Vasu Sharma, Soham Parekh
 * @brief testing mutex locks for one-one threads !
 * @version 0.1
 * @date 2021-04-21
 */
#include"vsthread.h"
#include<stdio.h>
#define NUM_THREADS 5
#define EVALUATE(val) (val >= -40 && val <=40)
#define COMPARE(val1,val2) (EVALUATE((val1) - (val2)) || EVALUATE((val2) - (val1)))
long long shared;
pthread_mutex lock;
int stop;
void *increment(void *arg){
    int *val = (int *)arg;
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        (*val)++;
    }
    return NULL;
}

long long sum(long long *arr){
    long long result = 0;
    int i = 0;
    for(i = 0; i < NUM_THREADS;i++)
        result += arr[i];
    return result;
}

int main(){
    long long locals[NUM_THREADS] = {0};
    pthread_init(ONE_ONE);
    pthread_t threads[5];
    pthread_mutex_init(&lock);
    int i;
    for(i = 0; i < NUM_THREADS;i++)
        pthread_create(threads +i,NULL,increment,locals + i);

    sleep(1);
    stop = 1;
    for(i = 0; i < NUM_THREADS;i++)
        pthread_join(threads[i],NULL);
    long long result = sum(locals);
    printf("Sum of local variables  = %lld\n",result);
    printf("Value of shared variable  = %lld\n",shared);
    if(COMPARE(shared,result))
        printf("TEST PASSED\n");
    else
        printf("TEST FAILED\n");
    
    return 0;
}