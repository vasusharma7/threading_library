/**
 * @file spin_test.c
 * @author Vasu Sharma, Soham Parekh
 * @brief testing spin locks for one-one threads !
 * @version 0.1
 * @date 2021-04-21
 */

#include"vsthread.h"
#include<stdio.h>
#define NUM_THREADS 5
#define EVALUATE(val) (((val) >= -NUM_THREADS) && ((val) <=NUM_THREADS))
#define COMPARE(val1,val2) ((EVALUATE((val1) - (val2))) || (EVALUATE((val2) - (val1))))
unsigned long long shared,t1,t2,t3,t4,t5;
int stop;
pthread_spinlock lock;
void *ret;

void * increment1(){
    while(!stop){
        pthread_spin_lock(&lock);
        shared++;
        pthread_spin_unlock(&lock);
        t1++;
    }
    return NULL;
}
void * increment2(){
    while(!stop){
        pthread_spin_lock(&lock);
        shared++;
        pthread_spin_unlock(&lock);
        t2++;
    }
    return NULL;
}

void * increment3(){
    while(!stop){
        pthread_spin_lock(&lock);
        shared++;
        pthread_spin_unlock(&lock);
        t3++;
    }
    return NULL;
}

void * increment4(){
    while(!stop){
        pthread_spin_lock(&lock);
        shared++;
        pthread_spin_unlock(&lock);
        t4++;
    }
    return NULL;
}

void * increment5(){
    while(!stop){
        pthread_spin_lock(&lock);
        shared++;
        pthread_spin_unlock(&lock);
        t5++;
    }
    return NULL;
}

int main(){
    
    pthread_t threads[NUM_THREADS];
    pthread_init(ONE_ONE);
    pthread_spin_init(&lock);
    pthread_create(&threads[0],NULL,increment1,NULL);
    pthread_create(&threads[1],NULL,increment2,NULL);
    pthread_create(&threads[2],NULL,increment3,NULL);
    pthread_create(&threads[3],NULL,increment4,NULL);
    pthread_create(&threads[4],NULL,increment5,NULL);
    sleep(1);
    stop=1;
    pthread_join(threads[0],NULL);
    pthread_join(threads[1],NULL);
    pthread_join(threads[2],NULL);
    pthread_join(threads[3],NULL);
    pthread_join(threads[4],NULL);

    printf("Sum of local variables  = %lld\n",t1+t2+t3+t4+t5);
    printf("Value of shared variable  = %lld\n",shared);
    if(shared == t1+t2+t3+t4+t5)
        printf("TEST PASSED\n");
    else
        printf("TEST FAILED\n");
    // exit(0);

    // for(i = 0; i < NUM_THREADS;i++)
    //     pthread_join(threads[i],NULL);
    
}