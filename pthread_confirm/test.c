/**
 * @file spin_test.c
 * @author Vasu Sharma, Soham Parekh
 * @brief testing mutex locks for threads !
 * @version 0.1
 * @date 2021-04-21
 */

#include<pthread.h>
#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>
#define NUM_THREADS 10
#define EVALUATE(val) (((val) >= -NUM_THREADS) && ((val) <=NUM_THREADS))
#define COMPARE(val1,val2) ((EVALUATE((val1) - (val2))) || (EVALUATE((val2) - (val1))))
unsigned long long shared,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10;
int stop;
pthread_mutex_t lock;
void *ret;

void * increment1(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t1++;
    }
    return NULL;
}
void * increment2(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t2++;
    }
    return NULL;
}

void * increment3(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t3++;
    }
    return NULL;
}

void * increment4(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t4++;
    }
    return NULL;
}

void * increment5(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t5++;
    }
    return NULL;
}

void * increment6(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t6++;
    }
    return NULL;
}

void * increment7(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t7++;
    }
    return NULL;
}

void * increment8(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t8++;
    }
    return NULL;
}

void * increment9(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t9++;
    }
    return NULL;
}

void * increment10(){
    while(!stop){
        pthread_mutex_lock(&lock);
        shared++;
        pthread_mutex_unlock(&lock);
        t10++;
    }
    return NULL;
}
int main(int argc,char **argv){
     if(argc < 2){
        printf("Insufficient Arguements! Required Arguments :-\n(1)Type of thread\n");
        exit(-1);
    }

    pthread_t threads[NUM_THREADS];
    // pthread_init(atoi(argv[1]));
    // pthread_mutex_init(&lock);
    pthread_create(&threads[0],NULL,increment1,NULL);
    pthread_create(&threads[1],NULL,increment2,NULL);
    pthread_create(&threads[2],NULL,increment3,NULL);
    pthread_create(&threads[3],NULL,increment4,NULL);
    pthread_create(&threads[4],NULL,increment5,NULL);
    pthread_create(&threads[5],NULL,increment6,NULL);
    pthread_create(&threads[6],NULL,increment7,NULL);
    pthread_create(&threads[7],NULL,increment8,NULL);
    pthread_create(&threads[8],NULL,increment9,NULL);
    pthread_create(&threads[9],NULL,increment10,NULL);
    // printf("%ld here\n",threads[0]);
    // pthread_exit(NULL);
    pthread_kill(threads[0],SIGSTOP);
    sleep(1);
    stop=1;
    pthread_join(threads[0],NULL);
    pthread_join(threads[1],NULL);
    pthread_join(threads[2],NULL);
    pthread_join(threads[3],NULL);
    pthread_join(threads[4],NULL);
    pthread_join(threads[5],NULL);
    pthread_join(threads[6],NULL);
    pthread_join(threads[7],NULL);
    pthread_join(threads[8],NULL);
    pthread_join(threads[9],NULL);
    printf("Sum of local variables  = %lld\n",t1+t2+t3+t4+t5+t6+t7+t8+t9+t10);
    printf("Value of shared variable  = %lld\n",shared);
    if(shared == t1+t2+t3+t4+t5+t6+t7+t8+t9+t10)
        printf("TEST PASSED\n");
    else
        printf("TEST FAILED\n");
}
