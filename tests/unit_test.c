/**
 * @file unit_test.c
 * @author Vasu Sharma,Soham Parekh
 * @brief Testing each and every function of threading library
 * @version 0.1
 * @date 2021-04-26
 *
 */

#include"vsthread.h"
#include<stdio.h>

#include <stdlib.h>

#define TYPE(a) (a == 1 ? "ONE_ONE" : "MANY_ONE")
int flag=1;
int stop = 0;
pthread_spinlock lock1;
pthread_mutex lock2;

void filler(){
    printf("-------------------------------------------------------\n\n");
}
pthread_t threads[5];

void* task1(){

    int sum = 0;
    for(int i = 0; i < 50; i++)
        sum += i;

}

void* task2(void* a){
  int *b = (int *)a;
  *b+=1;
  // printf("here\n");
  flag = 0;
  // printf("ret\n");
  return;
}


void *task3(){
  filler();
  printf("6 - Testing pthread_spin_lock\n");
  if(!pthread_spin_lock(&lock1))
      printf("pthread_spin_lock : TEST PASSED\n");
  else
      printf("pthread_spin_lock : TEST FAILED\n");

  filler();
  printf("7 - Testing pthread_spin_unlock\n");
  if(!pthread_spin_unlock(&lock1))
      printf("pthread_spin_unlock : TEST PASSED\n");
  else
      printf("pthread_spin_unlock : TEST FAILED\n");

  filler();
  printf("8 - Testing pthread_spin_destroy\n");
  if(!pthread_spin_destroy(&lock1))
      printf("pthread_spin_destroy : TEST PASSED\n");
  else
      printf("pthread_spin_destroy : TEST FAILED\n");

}

void* task4(){
  filler();
  printf("10 - Testing pthread_mutex_lock\n");
  if(!pthread_mutex_lock(&lock2))
      printf("pthread_mutex_lock : TEST PASSED\n");
  else
      printf("pthread_mutex_lock : TEST FAILED\n");

  filler();
  printf("11 - Testing pthread_mutex_unlock\n");
  if(!pthread_mutex_unlock(&lock2))
      printf("pthread_mutex_unlock : TEST PASSED\n");
  else
      printf("pthread_mutex_unlock : TEST FAILED\n");

  filler();
  printf("12 - Testing pthread_mutex_destroy\n");
  if(!pthread_mutex_destroy(&lock2))
      printf("pthread_mutex_destroy : TEST PASSED\n");
  else
      printf("pthread_mutex_destroy : TEST FAILED\n");
}

void* task5(void *arg){
    while(!stop);
    flag = 1;
}

void *task6(void *arg){
    while(!stop){
        if(flag){
            pthread_exit((void*)1);
        }
    }

}

void stop_thread(){
    stop = 1;
}


int main(int argc,char **argv){
    if(argc < 2){
        printf("Insufficient Arguments: Please provide the type of thread as second argument\n");
        exit(-1);
    }
    // int i; //supporting variables;
    int type = atoi(argv[1]);

    printf("Test: Testing pthread_init\n");
    if(pthread_init(type) == 0)
        printf("pthread_init : TEST PASSED\n");
    else
        printf("pthread_init : TEST FAILED\n");
    filler();

    printf("Test: Testing pthread_create\n");

    printf("1 - Creating a thread without arguments\n");

    if(pthread_create(&threads[0],NULL,task1,NULL) == 0)
        printf("pthread_create : TEST PASSED\n");
    else
        printf("pthread_create : TEST FAILED\n");
    filler();


    printf("2 - Creating a thread with arguments\n");
    int arg=10;
    if(pthread_create(&threads[1],NULL,task2,(void *)&arg) == 0)
        printf("pthread_create : TEST PASSED\n");
    else
        printf("pthread_create : TEST FAILED\n");
    filler();

    printf("3 - Creating a thread without start routine\n");
    if(pthread_create(&threads[2],NULL,NULL,NULL) != 0)
        printf("pthread_create : TEST PASSED\n");
    else
        printf("pthread_create : TEST FAILED\n");
    filler();


    printf("4 - Testing pthread_join\n");
    pthread_join(threads[1], NULL);
    // printf("%d\n",flag);
    if(!flag)
        printf("pthread_join : TEST PASSED\n");
    else
        printf("pthread_join : TEST FAILED\n");
    filler();

    printf("5 - Testing pthread_spin_init\n");
    if(!pthread_spin_init(&lock1))
        printf("pthread_spin_init : TEST PASSED\n");
    else
        printf("pthread_spin_init : TEST FAILED\n");

    pthread_create(&threads[2],NULL,task3,NULL);
    pthread_join(threads[2], NULL);


    filler();
    printf("9 - Testing pthread_mutex_init\n");
    if(!pthread_mutex_init(&lock2))
        printf("pthread_mutex_init : TEST PASSED\n");
    else
        printf("pthread_mutex_init : TEST FAILED\n");

    pthread_create(&threads[3],NULL,task4,NULL);
    pthread_join(threads[3], NULL);

    filler();
    printf("13 - Testing pthread_kill with SIGUSR1 signal\n");
    flag = 0;
    stop = 0;
    pthread_create(&threads[4],NULL,task5,NULL);
    signal(SIGUSR1,stop_thread);
    pthread_kill(threads[4],SIGUSR1);
    pthread_join(threads[4], NULL);
    if(flag == 1)
        printf("pthread_kill : TEST PASSED\n");
    else
        printf("pthread_kill : TEST FAILED\n");



    filler();
    printf("14 - Testing pthread_kill with invalid signal\n");
    flag = 0;
    stop = 0;
    pthread_create(&threads[4],NULL,task5,NULL);
    signal(SIGUSR1,stop_thread);

    if(pthread_kill(threads[4],-1) != 0)
        printf("pthread_kill : TEST PASSED\n");
    else
        printf("pthread_kill : TEST FAILED\n");
    pthread_kill(threads[4],SIGUSR1);
    pthread_join(threads[4], NULL);
    
    filler();
    printf("15 - Testing pthread_exit\n");
    flag = 0;
    stop = 0;
    pthread_create(&threads[4],NULL,task6,NULL);
    flag = 1;
    void *ret;
    pthread_join(threads[4], &ret);
    if(stop == 0)
        printf("pthread_exit : TEST PASSED\n");
    else
        printf("pthread_exit : TEST FAILED\n");
    
    filler();
    printf("16 - Testing pthread_exit return value from pthread_join\n");
    if((int)ret == 1)
        printf("pthread_exit : TEST PASSED\n");
    else
        printf("pthread_exit : TEST FAILED %d\n",(int)ret);

    filler();
    return 0;
}
