#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
#include "vsthread.h"
//This program assumes that the highest possible calculation will be within the bounds of long long int.

//global variable for storing result
long long int ** result;

//structure for passing as argument to thread
struct arg_struct {
    long long int **m1;
    long long int **m2;
    long long int row_upper_limit;
    long long int row_lower_limit;
    long long int o;
    long long int p;
};

//multiplies two matrices of given size
void *multiply(void *arguments){
  struct arg_struct *args;
  args = arguments; //casts void pointer to struct arg_struct*
  for(long long  int i = args->row_lower_limit;i < args->row_upper_limit;i++){
    for(long long  int j=0;j<args->p;j++){
      result[i][j]=0;
      for(long long int k=0;k<args->o;k++){
        *(*(result+i)+j)+= *(*(args->m1+i)+k) * *(*(args->m2+k)+j);
      }
    }
  }
}

//prints matrix
long long int check_matrix(long long int **matrix,long long int m,long long int n){
  // printf("\n\n%lld %lld\n",m,n);
  long long int sum = 0;
  for(long long int i=0;i<m;i++){
    for(long long int j=0;j<n;j++){
      sum+=matrix[i][j];
    }
  }
  return sum;
}

//allocates memory dynamically for matrix
long long int** alloc_matrix(long long int m,long long int n){
  long long int** matrix = (long long int**)malloc(sizeof(long long int*)*m);
  for (long long int i=0;i<m;i++){
    matrix[i] = (long long int*)malloc(sizeof(long long int)*n);
  }
  return matrix;
}

//reads matrix
void read_matrix(long long int **matrix,long long int m, long long int n){
  for(long long int i=0;i<m;i++){
    for(long long int j=0;j<n;j++){
      scanf("%lld",&matrix[i][j]);
    }
  }
}

int main(int argc, char **argv){
  long long int m,n,o,p;
  pthread_t a,b,c;
  pthread_init(atoi(argv[1]));
  struct arg_struct *args = malloc(sizeof(struct arg_struct)); //for passing argument to thread 1
  struct arg_struct *args1 = malloc(sizeof(struct arg_struct));//for passing argument to thread 2
  struct arg_struct *args2 = malloc(sizeof(struct arg_struct));//for passing argument to thread 3
  scanf("%lld %lld",&m,&n);
  long long int **mat1 = alloc_matrix(m,n);
  read_matrix(mat1,m,n);
  scanf("%lld %lld",&o,&p);
  if(n!=o){
    printf("Invalid multiplication, exiting.\n");  //if for two matrices m*n and o*p n!=o
    exit(-1);
  }
  long long int **mat2 = alloc_matrix(o,p);
  result = alloc_matrix(m,p);
  read_matrix(mat2,o,p);
  //setting values for lower limit and upper limit for thread 1
  args->row_lower_limit=0;
  args->row_upper_limit=m/3;
  args->o=o;
  args->p=p;
  args->m1=mat1;
  args->m2=mat2;
  args1->o=o;
  args1->p=p;
  args1->m1=mat1;
  args1->m2=mat2;
  //setting values for lower limit and upper limit for thread 2
  args1->row_lower_limit=args->row_upper_limit;
  args1->row_upper_limit=m/3+(m/3);
  args2->o=o;
  args2->p=p;
  args2->m1=mat1;
  args2->m2=mat2;
  //setting values for lower limit and upper limit for thread 3
  args2->row_lower_limit=args1->row_upper_limit;
  args2->row_upper_limit=m;
  pthread_create(&a, NULL, multiply, (void *)args);  //thread1
  pthread_create(&b, NULL, multiply, (void *)args1); //thread2
  pthread_create(&c, NULL, multiply, (void *)args2); //thread3
  pthread_join(a,NULL); //waiting for thread 1
  pthread_join(b,NULL); //waiting for thread 2
  pthread_join(c,NULL); //waiting for thread 3
  if(check_matrix(result,m,p)==1000000000)printf("TEST PASSED\n");
  else printf("FAILED\n");
  return 0;
}
