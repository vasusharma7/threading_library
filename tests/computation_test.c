/**
 * @file stress_test.c
 * @author Vasu Sharma,Soham Parekh
 * @brief Doing a compuatationally expensive task (checking if a larg number is prime ) faster using threads.It takes 11 seconds to do it without threads
 * @version 0.1
 * @date 2021-04-25
 * 
 * 
 */
// unsigned long long prime = 1111111111111111111;
unsigned long long prime = 1111111111111111111;
int term = 0;
void *ret;
#include"vsthread.h"
#include<stdio.h>
#define LEAP 1000000
#define MIN(A,B) ((A) < (B) ? (A) : (B))

struct arg {
    unsigned long long start;
    unsigned long long end;
};
void *isPrime(void *arg){
    struct arg *inst = (struct arg*)arg;

    for(unsigned long long num = inst->start; num <= inst->end; num++){
        if(term == 1){
            pthread_exit(ret);
        }
        if(prime % (num) == 0){
            term = 1;
            printf("%lld is Not Prime,divisible by %lld\n",prime,num);
        }
    }
}

int main (int argc, char **argv) {
   pthread_init(atoi(argv[1]));
    pthread_t threads[100];
    int count = 0;
    unsigned long long i=2;
    for(i = 2; i*i <= prime;i+=MIN(i+LEAP,i*i)){
        struct arg *inst = (struct arg*)calloc(1,sizeof(struct arg));
        inst->start = i;
        inst->end=MIN(i+LEAP,i*i);
        pthread_create(&threads[count++],NULL,isPrime,(void *)inst);
    }
    printf("Number of threads = %d\n",count-1);
    while(count > 0){
        pthread_join(threads[--count],NULL);
    }
    if(!term)
        printf("%lld is prime\n",prime);

}