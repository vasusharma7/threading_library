#include <stdio.h>
#include "vsthread.h"

#define number_of_threads 64
int num;
void *PrintHello(void *threadid) {
   // int *tid=(int*)threadid;
   // printf("Running Thread with Id = %d\n", *tid);
   for(long long int j=0;j<1000000;j++);
   num++;
   pthread_exit(NULL);
}

int main (int argc, char **argv) {
   pthread_init(atoi(argv[1]));
   pthread_t threads[number_of_threads];
   int rc=0;
   int i;
   for( i = 0; i < number_of_threads; i++ ) {
      // printf("main() : creating thread %d\n",i);
      rc = pthread_create(&threads[i], NULL, PrintHello,(void *)&threads[i]);
      if(rc){
         printf("Error:unable to create thread, %d\n", rc);
         exit(-1);
      }
      // for(long long int j=0;j<1000000;j++);
   }
   for(i=0;i<number_of_threads;i++)
    pthread_join(threads[i],NULL);
   // pthread_exit(NULL);
   if(num == number_of_threads){
      printf("CREATED %d THREADS\nTEST PASSED\n",num);
   }
   else
      printf("TEST FAILED\n");

}
