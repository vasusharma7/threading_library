one_one(){
    printf "\nRUNNING TESTS FOR ONE-ONE THREADS\n\n"

    printf "\nRunning Unit Test\n\n"
    ./bin/unit_test 1

    printf "\nRunning Thread Create Stress Test\n\n"
    ./bin/create_test 1

    printf "\nRunning MutexLock Test\n\n"
    ./bin/mutex_test 1

    printf "\nRunning SpinLock Test\n\n"
    ./bin/spin_test 1

    printf "\nRunning Matrix Multiplication Test\n"
    ./bin/matrix_test 1 < input.txt

    printf "\nRunning Computation Test using multiple threads\n\n"
    time -p ./bin/computation_test 1
    printf "\nRun time ./bin/prime_test for primality test without threads. Not included in this testing.\n"
}
many_one(){
    printf "\nRUNNING TESTS FOR MANY-ONE THREADS\n\n"

    printf "\nRunning Unit Test\n\n"
    ./bin/unit_test 0

    printf "\nRunning Thread Create Stress Test\n\n"
    ./bin/create_test 0

    printf "\nRunning MutexLock Test\n\n"
    ./bin/mutex_test 0

    printf "\nRunning SpinLock Test\n\n"
    ./bin/spin_test 0

    printf "\nRunning Matrix Multiplication Test\n"
    ./bin/matrix_test 0 < input.txt

    printf "\nRunning Computation Test using multiple threads\n\n"
    time -p ./bin/computation_test 0
    printf "\nRun time ./bin/prime_test for primality test without threads. Not included in this testing.\n"

}
make
option="${1}"
case ${option} in
   --one-one)
      printf "\nExecuting Tests for One-One Threads\n"
      one_one
      exit
      ;;
   --many-one)
      printf "\nExecuting Tests for Many-One Threads\n"
      many_one
        exit
      ;;
   *)
       printf "\nExecuting Tests for One-One Threads\n\n"
       one_one
       printf "\nExecuting Tests for Many-One Threads\n\n"
       many_one
      ;;
esac
