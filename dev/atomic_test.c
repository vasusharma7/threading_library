#include<stdio.h>
#include"vsthread.h"
typedef long long ll;

ll c,c1,c2;
pthread_spinlock lock;
pthread_mutex mut;
enum Test1 {SPIN, MUTEX};
enum Test2 {M21, O1};
int test1 = MUTEX;
int test2 = O1;
void *temp;
ll thresh = 1000000;
void* inc1(){
    while (c < thresh){
        test1 == SPIN ? pthread_spin_lock(&lock) : pthread_mutex_lock(&mut);
        c++;
        test1 == SPIN ? pthread_spin_unlock(&lock) : pthread_mutex_unlock(&mut);
        c1++;
        // pthread_exit(temp);
        
    }
    printf("over and out 1\n");
}
void* inc2(){
    while (c < thresh/2){

        test1== SPIN ? pthread_spin_lock(&lock) : pthread_mutex_lock(&mut);
        c++;
        test1 == SPIN ? pthread_spin_unlock(&lock) : pthread_mutex_unlock(&mut);
        c2++;
        // void *temp;
        pthread_exit(temp);
    }
    printf("over and out 2\n");

}

int main(){
    pthread_t t1,t2;
    // pthread_init();
    pthread_spin_init(&lock);
    pthread_mutex_init(&mut);
    pthread_create(&t1,NULL,inc1,NULL);
    pthread_create(&t2,NULL,inc2,NULL);

    pthread_join(t1,NULL);
    pthread_join(t2,NULL);
    // while(c < thresh);
    // sleep(1);
    printf("c = %lli\tc1+c2= %lli\tc1= %lli\tc2= %lli\n",c,c1+c2,c1,c2);
    return 0;
}
