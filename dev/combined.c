#define _GNU_SOURCE
#include "vsthread.h"
#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)


#define JB_SP 6
#define JB_PC 7
/**
 * @brief This function encrypts(mangles) the address, necessary before storing it in jump_buffer(context)
 * 
 * @param p The address to mangle(encrypt)
 * @return long int Mangled address
 */
long int mangle(long int p) {
    long int ret;
    asm(" mov %1, %%rax;\n"
        " xor %%fs:0x30, %%rax;"
        " rol $0x11, %%rax;"
        " mov %%rax, %0;"
        : "=r"(ret)
        : "r"(p)
        : "%rax"
    );
    return ret;
}

//common
/**
 * @brief atomically(in a single instruction `xadd`) add `incr` value to the value stored at location pointed by `p`
 * 
 * @param p pointer to the location to increment
 * @param incr the value to add
 * @return unsigned long int the result after addition
 */
unsigned long int atomic_add(volatile unsigned long int* p, unsigned long int incr){

    unsigned long int result;
    __asm__ __volatile__ ("lock; xadd %0, %1" :
            "=r"(result), "=m"(*p):
            "0"(incr), "m"(*p) :
            "memory");
			//clobers give extra information to compilers
			//here memory clobber tells compiler that we are going to access memory other than that mentioned in input/output operands - ie. accessing memory pointed by p
    return result;
}

//common
/**
 * @brief handle signals in many-one threads 
 * 
 * @param signum SIGNAL number of the signal handled 
 * @todo change signal to sigaction for better control
 */
void signal_handler(int signum){
	if(signum==SIGALRM){
		// printf("got alarm signal\n");
		scheduler();
	}
	else{
		printf("random signal\n");
	}
}

/**
 * @brief Allocate stack for threads
 * @param size size of the stack to allocate
 * @return void* (starting address + size ) address of the stack.
 * common function
 */

void *createStack(int size){

	void *stack = mmap(NULL, size, PROT_READ | PROT_WRITE,
					   MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
	if (stack == MAP_FAILED)
		exit(2);

	return stack + size;
}

/**
 * @brief Get the running process in one-one threads
 *
 * @return thread_t* pointer to the running process thread block
 */

thread_t* get_running_process(){
	pthread_t tid = gettid();
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->tid==tid)
      return thread_pool[i];
  }
	return NULL;
}

/**
 * @brief Get the running/scheduled process in many-one threads
 * 
 * @return thread_t* pointer to the thread block of running process 
 */
thread_t* m1_get_running_process(){
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNING)
      return thread_pool[i];
  }
	// return previous_thread;
	return NULL;
}

//common
/**
 * @brief Get the first runnable process from the thread_pool
 * @todo check if its used any where ?
 * @return thread_t* pointer to the thread control block of RUNNABLE process
 */
thread_t* get_runnable_process(){
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNABLE)
      return thread_pool[i];
  }
	return NULL;
}
/**
 * @brief get last running process to be scheduled from the thread pool
 * @todo write the return value
 * @return int //
 */
int last_running_process(){
  // printf("z\n");
  int count = 0;
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNABLE || thread_pool[i]->state==RUNNING)
      count++;
  }
  if(count>=2){
    return 0;
  }
	return 1;
}
/**
 * @brief free up the resources held by a thread like stack space, space for storing context etc.
 * 
 * @param thread thread control block of the thread whose resources are to be freed
 */
void freeup(thread_t *thread){
  return;
	// printf("this -> %d %p\n",i++,thread->context);
	free(thread->context);
	// printf("this -> %d %p\n",i++,thread->stack);
	munmap(thread->stack - STACK_SIZE,STACK_SIZE);
	// printf("this -> %d %p\n",i++,thread);
	free(thread);
}
/**
 * @brief Find a terminated process and free up it's resources
 * 
 * @return int Index of the first TERMINATED process found from thread_pool array.
 */
int cleanup(){
	int min = -1;
	for(int i =0; i< curr_thread_index;i++){
		if(thread_pool[i]->state == TERMINATED){
			freeup(thread_pool[i]);
			thread_pool[i]->state =EMBYRO;
			if(min == -1)
			min = i;
		}
		else if(thread_pool[i]->state == EMBYRO){
			if(min > i)
			min = i;
		}
	}
	return min;
}
/**
 * @brief(ONE-ONE THREADS) The entry point of execution of all the threads. This acts like a wrapper function to execute user function. 
 * 
 * @param arg Pointer to `execute_argument` structure containing thread to execute,user fucnction to execute in thread, its arguements etc.
 * @todo check return(as of now)
 * @return int* always returns NULL 
 */
int *pthread_execute(void *arg){
	execute_argument *args = (execute_argument *)arg;
	thread_t *thread = (thread_t*)args->thread;
	thread->state = RUNNING;
	if(setjmp(thread->exit_context) == 0){
		args->function(args->arg);
	}
	// printf("before join...%ld\n",thread->tid);
	thread->state = TERMINATED;
	return NULL;
}

//for many to one
/**
 * @brief(MANY-ONE THREADS) The entry point of execution of all the threads. This acts like a wrapper function to execute user function. 
 * 
 * @param arg Pointer to `execute_argument` structure containing user fucnction to execute in thread, its arguements etc.
 * @return doesnt return anything, calls scheduler at the end
 */
void m1_pthread_execute (){
	thread_t *thread = thread_pool[fired_thread];
	execute_argument *args = (execute_argument *)thread->exe;
	if(setjmp(thread->exit_context)==0){
		args->function(args->arg);
	}
	ualarm(0,0);
	thread->state = TERMINATED;
	previous_thread = thread;
	scheduler();
}
/**
 * @brief To select one of the threads from thread_pool and start its execution.Used in Many-One threads
 * Method used - round robin scheduling.
 * Called by SIGARLM signal handler (timer interrupt), from mutext locks,after a thread completes execution etc.
 * @return This function never returns, jumps to the context of the selected process for scheduling
 */
void scheduler(){
	ualarm(0,0);
	// printf("hi %d\n",curr_thread_index);
  thread_t *current_process = m1_get_running_process();

  if(current_process==NULL){
    current_process = previous_thread;
  }
	if (current_process->state!=TERMINATED && current_process->state!=EMBYRO){
    // printf("making %d RUNNABLE\n",current_process->tid);
    // sleep(1);
		current_process->state = RUNNABLE;
	}
  //   printf("current_process = %p\ttid = %ld",current_process,current_process->tid);
  //   printf("hi2 %d\n",curr_thread_index);


	thread_t *next_thread;
	// printf("finding prc\n");
	int counter = 1;
	do{
		if((current_process->tid)+counter == curr_thread_index){
		  next_thread = thread_pool[0];
		}
		else{
			next_thread=thread_pool[(current_process->tid)+counter];
		}
		// printf("next_thread = %ld, status = %d\n",next_thread->tid,next_thread->state);
		counter+=1;
	}while(next_thread->state != RUNNABLE && next_thread->state != RUNNING);
	if(next_thread->tid==current_process->tid && previous_thread!=NULL){
		current_process=thread_pool[curr_thread_index-1];
	}
  	next_thread->state = RUNNING;
	signal(SIGALRM,signal_handler);
	// if(!last_running_process()){
	// 	ualarm(TIMER,0);
	// }
	// else{
	// 	printf("Last process\n");
	// }
  // printf("scheduler is calling %ld tid, swapping %ld tid\n",next_thread->tid,current_process->tid);

  previous_thread = NULL;
  if(sigsetjmp(current_process->context,1) == 2)
    return;

  	// printf("in sched\n");
	//   printf("PC = %ld\tSP = %ld\n",next_thread->context[0].__jmpbuf[JB_PC],next_thread->context[0].__jmpbuf[JB_SP]);
	//   printf("context = %p\n",next_thread->context);

  ualarm(TIMER,0);
  // if (swapcontext(current_process->context, next_thread->context) == -1)
  fired_thread = next_thread->tid;
  siglongjmp(next_thread->context,2);
  handle_error("swapcontext");
}

/**
 * @brief Initialise pthread library with type of thread to be used, setup internal data structures etc.
 * 
 * @param mode type of threads to be used by the process - allowed modes {ONE_ONE, MANY_ONE}(see vsthread.h)
 */
void pthread_init(int mode){
  if(mode==ONE_ONE){
    GLOBAL_MODE=ONE_ONE;
    return;
  }
  else{
    GLOBAL_MODE=MANY_ONE;
  }
	/* ----------- add main threads to the list of threads to be scheduled ------ */
	bitmap_contexts = (int*)malloc(sizeof(int)*64);
	for(int i=0;i<64;i++){
		bitmap_contexts[i] = 0;
	}
	curr_thread_index = 0;
	bitmap_contexts[0] = 1;
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	thread->state = RUNNING;
	thread->tid = curr_thread_index;
	// thread->context = (ucontext_t *)malloc(sizeof(ucontext_t));
	// getcontext(thread->context);
  // printf("here2\n");
  // thread->context = malloc(sizeof(jmp_buf));
  //   jmp_buf temp;
  sigsetjmp(thread->context,1);
  //   thread->context = &temp;
	thread_pool[curr_thread_index++] = thread;
	signal(SIGALRM,signal_handler);
	// ualarm(TIMER,0);
}


/**
 * @brief Creates a new one-one thread
 * 
 * @param newthread The location to store the thread Id of newly created thread
 * @param attr attribtes for creating new thread
 * @param start_routine the location(function pointer) to start execution of newly created thread(entry point)
 * @param arg The only argument to be passed to the start routine
 * @return int On succcessful creation returns 0, else returns appropirate ERRNO
 * @todo error handling
 */
extern int oo_pthread_create(pthread_t *newthread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg){
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	execute_argument *ex_thread = (execute_argument *)malloc(sizeof(execute_argument));
	ex_thread->arg = arg;
	ex_thread->function = start_routine;
	ex_thread->thread = thread;
	thread->stack = createStack(STACK_SIZE);
	thread->state = RUNNABLE;
	unsigned long int flags = CLONE_THREAD | CLONE_SIGHAND | CLONE_VM | CLONE_IO | CLONE_FILES | CLONE_FS | SIGCLD;
	thread->tid = clone((int (*)(void *))pthread_execute, thread->stack, flags, (void *)ex_thread);
	thread_pool[curr_thread_index++] = thread;
	*newthread = thread->tid;
  // printf("done\n");
	return 0;
}
/**
 * @brief Creates a new many-one thread
 * 
 * @param newthread The location to store the thread Id of newly created thread
 * @param attr attribtes for creating new thread
 * @param start_routine the location(function pointer) to start execution of newly created thread(entry point)
 * @param arg The only argument to be passed to the start routine
 * @return int On succcessful creation returns 0, else returns appropirate ERRNO
 * @todo error handling
 */
extern int m1_pthread_create(pthread_t *newthread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg){
  	ualarm(0,0);
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	execute_argument *ex_thread = (execute_argument *)malloc(sizeof(execute_argument));
	ex_thread->arg = arg;
	ex_thread->function=start_routine;

	// printf("%p\n",ex_thread->function);
	thread->stack = createStack(STACK_SIZE);
	thread->exe = ex_thread;
	thread->state = RUNNABLE;
	int min;

	if ((min =cleanup()) != -1){
		thread->tid = min;
	}
	else{
		thread->tid = curr_thread_index;
	}

  sigsetjmp(thread->context,1);
	thread->context[0].__jmpbuf[JB_SP] = mangle((long int)thread->stack);
	thread->context[0].__jmpbuf[JB_PC] = mangle((long int)m1_pthread_execute);
	// printf("PC = %ld\tSP = %ld\n",thread->context[0].__jmpbuf[JB_PC],thread->context[0].__jmpbuf[JB_SP]);
	// printf("context = %p\n",thread->context);
	thread_pool[min != -1 ? min : curr_thread_index++] = thread;
	*newthread = thread->tid;
  ualarm(TIMER,0);
	return 0;
}

//wrapper function
/**
 * @brief The wrapper function around {one-one pthread_create} and {many-one pthread_create}.
 * Directs control to appropirate function based on how the library is initialised in pthread_init
 * 
 * @param newthread The location to store the thread Id of newly created thread
 * @param attr attribtes for creating new thread
 * @param start_routine the location(function pointer) to start execution of newly created thread(entry point)
 * @param arg The only argument to be passed to the start routine
 * @return int On succcessful creation returns 0, else returns appropirate ERRNO
 * @todo error handling
 */
extern int pthread_create(pthread_t *newthread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_create(newthread,attr,start_routine,arg);
  }
  else{
    return m1_pthread_create(newthread,attr,start_routine,arg);
  }
}

/**
 * @brief To stop the execution of the caller many-one thread.
 * 
 * @param retval Pointer to the return value to be stored in thread control block
 * This value is available to another thread in the same process that calls pthread_join().
 */
void m1_pthread_exit(void *retval){
	ualarm(0,0);
	thread_t *curr_thread=thread_pool[fired_thread];
	curr_thread->retval = retval;
	if(curr_thread->tid == 0)
		exit(0);
	longjmp(curr_thread->exit_context,1);

}
/**
 * @brief To terminate the caller one-one thread.
 * 
 * @param retval Pointer to the return value to be stored in thread control block
 * This value is available to another thread in the same process that calls pthread_join().
 */
void oo_pthread_exit(void *retval){
	thread_t *thread = get_running_process();
  //if it is NULL then only running process is main thread
  if(thread==NULL){
    exit(0);
  }
	thread->retval = retval;
  // printf("calling longjmp\n");
	longjmp(thread->exit_context,1);
}
/**
 * @brief Wrapper function around {one-one pthread_exit} and {many_one pthread_exit}.
 * Passes control to appropirate function based on how the library is initialised in pthread_init
 * 
 * @param retval The exit status of the caller thread
 */
void pthread_exit(void *retval){
  if(GLOBAL_MODE == ONE_ONE){
    oo_pthread_exit(retval);
  }
  else{
    m1_pthread_exit(retval);
  }
}
/**
 * @brief This function waits for the many-one thread specified by threadid to terminate.
 * 
 * @param threadid Thread Id of the target many-one thread to wait for.
 * @param __thread_return If __thread_return is not NULL, then it copies the exit status of the target  thread  (i.e.,the value that the target thread supplied to pthread_exit()) into the location pointed to by __thread_return.
 * @return int On success, it returns 0; on error, it returns an error number.
 */
int m1_pthread_join(pthread_t threadid, void **__thread_return){
	// printf("im here %p\n",&threadid);
	while(1){
		if(thread_pool[threadid]->state == TERMINATED){
      		// printf("returning to main\n");
			if(__thread_return)
				*__thread_return = thread_pool[threadid]->retval;
			return 0;
		}
	}
	return 0;
}
/**
 * @brief This function waits for the one-one thread specified by threadid to terminate.
 * 
 * @param threadid Thread Id of the target one-one thread to wait for.
 * @param __thread_return If __thread_return is not NULL, then it copies the exit status of the target  thread  (i.e.,the value that the target thread supplied to pthread_exit()) into the location pointed to by __thread_return.
 * @return int On success, it returns 0; on error, it returns an error number.
 */
int oo_pthread_join(pthread_t threadid, void **__thread_return){
	int i;
	for(i=0;i<curr_thread_index;i++){
		if(thread_pool[i]->tid==threadid)
		break;
	}
	while(1){
		if(thread_pool[i]->state == TERMINATED){
			if(__thread_return)
				*__thread_return = thread_pool[threadid]->retval;
			return 1;
		}
	}
	return 0;
}
/**
 * @brief The wrapper function around {one-one pthread_join} and {many-one pthread_join}.
 * Passes control to appropirate function based on how the library is initialised while calling pthread_init.
 * 
 * @param threadid Thread Id of the target many-one thread to wait for.
 * @param __thread_return If __thread_return is not NULL, then it copies the exit status of the target  thread  (i.e.,the value that the target thread supplied to pthread_exit()) into the location pointed to by __thread_return.
 * @return int On success, it returns 0; on error, it returns an error number.
 */
int pthread_join(pthread_t threadid, void **__thread_return){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_join(threadid,__thread_return);
  }
  else{
    return m1_pthread_join(threadid,__thread_return);
  }
}
/**
 * @brief Initialise spin lock for many-one threads.
 * 
 * @param lock The lock variable to initialise in unlocked state
 * @return int On success, return 0, else return ERROR NO.
 * @todo check if spin lock is already initialsed ( and other error checking ....) 
 */
int m1_pthread_spin_init(pthread_spinlock *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}
/**
 * @brief Initialise spin lock for one-one threads.
 * 
 * @param lock The lock variable to initialise in unlocked state
 * @return int On success, return 0, else return ERROR NO.
 * @todo check if spin lock is already initialsed ( and other error checking ....) 
 */
int oo_pthread_spin_init(pthread_spinlock *lock){
	// atomic_init(&lock->ticket,0);
	// atomic_init(&lock->turn,0);
	lock->ticket = 0;
	lock->turn = 0;
	return 0;
}
/**
 * @brief Wrapper function for {one_one pthread_spin_init} and {many_one pthread_init}
 * This function allocates any resources required for the use of the spin lock referred to by lock and initializes the lock to be in the unlocked state.
 * 
 * @param lock The lock variable to initialise in unlocked state.
 * @return int On success, return 0, else return ERROR NO.
 * @todo check if spin lock is already initialsed ( and other error checking ....)
 */
int pthread_spin_init(pthread_spinlock *lock){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_spin_init(lock);
  }
  else{
    return m1_pthread_spin_init(lock);
  }
}

/**
 * @brief This function destroys a previously initialized spin lock in many-one threads, freeing any resources that were allocated for that lock.
 *  
 * @param lock The lock to destroy.
 * @return int On success, return 0, else return ERROR NO.
 * @todo error checking .... OR eliminate this function - does no useful work
 */

int m1_pthread_spin_destroy(pthread_spinlock *lock){
	return 0;
}

/**
 * @brief This function destroys a previously initialized spin lock in one-one threads, freeing any resources that were allocated for that lock.
 *  
 * @param lock The lock to destroy.
 * @return int On success, return 0, else return ERROR NO.
 * @todo error checking ....OR eliminate this function - does no useful work
 */

int oo_pthread_spin_destroy(pthread_spinlock *lock){
	return 0;
}
/**
 * @brief The wrapper function around {one-one pthread_spin_destroy} and {many-one pthread_spin_destroy}
 *  
 * @param lock The lock to destroy.
 * @return int On success, return 0, else return ERROR NO.
 * @todo error checking ....
 */
int pthread_spin_destroy(pthread_spinlock *lock){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_spin_destroy(lock);
  }
  else{
    return m1_pthread_spin_destroy(lock);
  }
}


/**
 * @brief This function locks the spin lock referred to by lock in many-one threads.
 * 
 * @param lock The spinlock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */

int m1_pthread_spin_lock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = m1_get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn);
	return 0;
}

/**
 * @brief This function unlocks the spin lock referred to by lock in many-one threads.
 * 
 * @param lock The spinlock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */

int m1_pthread_spin_unlock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = m1_get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->turn,1);

	return 0;
}

/**
 * @brief This function locks the spin lock referred to by lock in one-one threads.
 * 
 * @param lock The spinlock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */
int oo_pthread_spin_lock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn);
	return 0;
}
/**
 * @brief This function unlocks the spin lock referred to by lock in one-one threads.
 * 
 * @param lock The spinlock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */
int oo_pthread_spin_unlock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->turn,1);

	return 0;
}

/**
 * @brief This function acts as a wrapper function around {one-one pthread_spin_lock} and {many-one pthread_spin_lock}
 * 
 * @param lock The spinlock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */

int pthread_spin_lock(pthread_spinlock *lock){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_spin_lock(lock);
  }
  else{
    return m1_pthread_spin_lock(lock);
  }
}
/**
 * @brief This function acts as a wrapper function around {one-one pthread_spin_unlock} and {many-one pthread_spin_unlock}
 * 
 * @param lock The spinlock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */

int pthread_spin_unlock(pthread_spinlock *lock){
  if(GLOBAL_MODE == ONE_ONE){
    oo_pthread_spin_unlock(lock);
  }
  else{
    m1_pthread_spin_unlock(lock);
  }
}

/**
 * @brief Initialise mutex lock for many-one threads.
 * 
 * @param lock The lock variable to initialise in unlocked state
 * @return int On success, return 0, else return ERROR NO.
 * @todo check if mutex lock is already initialsed ( and other error checking ....) 
 */

int m1_pthread_mutex_init(pthread_mutex *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}

/**
 * @brief This function locks the mutex lock referred to by lock in many-one threads.
 * 
 * @param lock The mutex lock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */
int m1_pthread_mutex_lock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	// if(thread->tid == 2)
	// printf("Incrementing ticket for %ld process\n",thread->tid);
	while (lock->turn != thread->myturn){
		// printf("i didn't get the lock :( %ld %ld %ld %ld\n",thread->tid,thread->myturn,lock->ticket,lock->turn);
		scheduler();
	}
	return 0;
}
/**
 * @brief This function unlocks the mutex lock referred to by lock in many-one threads.
 * 
 * @param lock The mutex lock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */
int m1_pthread_mutex_unlock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	atomic_add(&lock->turn,1);
		// if(thread->tid == 2)
	// printf("releasing ticket for %ld process\n",thread->tid);

	// lock->turn += 1;

	return 0;
}


/**
 * @brief Initialise mutex lock for one-one threads.
 * 
 * @param lock The lock variable to initialise in unlocked state
 * @return int On success, return 0, else return ERROR NO.
 * @todo check if mutex lock is already initialsed ( and other error checking ....) 
 */

int oo_pthread_mutex_init(pthread_mutex *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}
/**
 * @brief This function locks the mutex lock referred to by lock in one-one threads.
 * 
 * @param lock The mutex lock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */
int oo_pthread_mutex_lock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn){
		sched_yield();
	}
	return 0;
}

/**
 * @brief This function unlocks the mutex lock referred to by lock in many-one threads.
 * 
 * @param lock The mutex lock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */

int oo_pthread_mutex_unlock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	atomic_add(&lock->turn,1);

	return 0;
}

/**
 * @brief This function acts as a wrapper function around {one-one pthread_mutex_init} and {many-one pthread_mutex_init}
 * 
 * @param lock The lock variable to initialise in unlocked state.
 * @return int On success, return 0, else return ERROR NO.
 */

int pthread_mutex_init(pthread_mutex *lock){
  if(GLOBAL_MODE == ONE_ONE){
    return oo_pthread_mutex_init(lock);
  }
  else{
    return m1_pthread_mutex_init(lock);
  }
}

/**
 * @brief This function acts as a wrapper function around {one-one pthread_mutex_lock} and {many-one pthread_mutex_lock}
 * 
 * @param lock The mutex lock to lock.
 * @return int On success, return 0, else return ERROR NO.
 */

int pthread_mutex_lock(pthread_mutex *lock){
  if(GLOBAL_MODE == ONE_ONE){
    oo_pthread_mutex_lock(lock);
  }
  else{
    m1_pthread_mutex_lock(lock);
  }
}


/**
 * @brief This function acts as a wrapper function around {one-one pthread_mutex_unlock} and {many-one pthread_mutex_unlock}
 * 
 * @param lock The mutex lock to unlock.
 * @return int On success, return 0, else return ERROR NO.
 */


int pthread_mutex_unlock(pthread_mutex *lock){
  if(GLOBAL_MODE == ONE_ONE){
    oo_pthread_mutex_unlock(lock);
  }
  else{
    m1_pthread_mutex_unlock(lock);
  }
}