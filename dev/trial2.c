#include"vsthread.h"
// #include<pthread.h>
#include<stdio.h>
#include<unistd.h>
#include<malloc.h>
#include<stdlib.h>
// pthread_mutex lock;
pthread_spinlock lock;

long long c,c1,c2;
void *thread1(){
  while (1){
  // while(c != 19000000){
    // pthread_mutex_lock(&lock);
    pthread_spin_lock(&lock);

    // printf("I am inside lock 1\n");
    c++;
    // pthread_mutex_unlock(&lock);
    pthread_spin_unlock(&lock);
    c1++;
  }
}

void *thread2(){
  // while (c != 190000000){
  while (1){
    pthread_spin_lock(&lock);
    // pthread_mutex_lock(&lock);
    // printf("I am inside lock 2\n");
    c++;
    pthread_spin_unlock(&lock);
    // pthread_mutex_unlock(&lock);
    c2++;
  }
}

// void *printValues(){
//   while(1){
//     printf("c = %lld, c1 + c2 = %lld, c1 = %lld, c2=%lld\n",c,c1+c2,c1,c2);
//     sleep(1);
//   }

// }

int main(int argc, char **argv){
  pthread_t a,b,p;
  
  // pthread_init();
  // if (pthread_mutex_init(&lock) != 0) {
  if (pthread_spin_init(&lock) != 0) {
        printf("\n mutex init has failed\n");
        return 1;
    }
  pthread_create(&a,NULL,thread1,NULL);
  pthread_create(&b,NULL,thread2,NULL);
  pthread_create(&p,NULL,thread2,NULL);
  
  while(c < 1000000){
    // printf("c = %lld, c1 + c2 = %lld, c1 = %lld, c2=%lld\n",c,c1+c2,c1,c2);
    continue;
  }
  // sleep(10);
  // pthread_join(a,NULL);
  // pthread_join(b,NULL);

  printf("c = %lld, c1 + c2 = %lld, c1 = %lld, c2=%lld\n",c,c1+c2,c1,c2);

  return 0;
}
