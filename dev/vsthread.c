#define _GNU_SOURCE
#include "vsthread.h"

unsigned long int atomic_add(volatile unsigned long int* p, unsigned long int incr){

    unsigned long int result;
    __asm__ __volatile__ ("lock; xadd %0, %1" :
            "=r"(result), "=m"(*p):
            "0"(incr), "m"(*p) :
            "memory");
    return result;
}

/**
 * @brief Allocate stack for threads
 * @param size size of the stack to allocate
 * @return void* (starting address + size ) address of the stack.
 */

void *createStack(int size){

	void *stack = mmap(NULL, size, PROT_READ | PROT_WRITE,
					   MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
	if (stack == MAP_FAILED)
		exit(2);

	return stack + size;
}

/**
 * @brief Get the running process object
 * 
 * @return thread_t* pointer to the running 
 */

thread_t* get_running_process(){
	pthread_t tid = gettid();
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->tid==tid)
      return thread_pool[i];
  }
	return NULL;
}

int *pthread_execute(void *arg){
	execute_argument *args = (execute_argument *)arg;
	thread_t *thread = (thread_t*)args->thread;
	thread->state = RUNNING;
	if(setjmp(thread->exit_context) == 0){
		args->function(args->arg);
	}
	printf("before join...%ld\n",thread->tid);
	thread->state = TERMINATED;
	
	// pthread_t tid = gettid();
	// for(int i=0;i<curr_thread_index;i++){
	// 	if(thread_pool[i]->tid==tid)
	// 	thread_pool[i]->state=TERMINATED;
	// }
	// pthread_exit(NULL);
	return NULL;
}

extern int pthread_create(pthread_t *newthread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg){
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	execute_argument *ex_thread = (execute_argument *)malloc(sizeof(execute_argument));
	ex_thread->arg = arg;
	ex_thread->function = start_routine;
	ex_thread->thread = thread;
	thread->stack = createStack(STACK_SIZE);
	thread->state = RUNNABLE;
	unsigned long int flags = CLONE_THREAD | CLONE_SIGHAND | CLONE_VM | CLONE_IO | CLONE_FILES | CLONE_FS | SIGCLD;
	thread->tid = clone((int (*)(void *))pthread_execute, thread->stack, flags, (void *)ex_thread);
	thread_pool[curr_thread_index++] = thread;
	*newthread = thread->tid;
	return thread->tid;
}

pthread_t pthread_self(void){
	pthread_t x = syscall(__NR_gettid);
	return x;
}

void pthread_exit(void *retval){
	thread_t *thread = get_running_process();
	thread->retval = retval;
	longjmp(thread->exit_context,1);
}

int pthread_join(pthread_t threadid, void **__thread_return){
	// int status;
	// waitpid(threadid, &status, __WALL);
	// while (1){
	// 	waitpid(threadid, &status, __WALL);
	// 	if (WIFEXITED(status)) // assume it will exit at some point
	// 		break;
	// }
	int i;
	for(i=0;i<curr_thread_index;i++){
		if(thread_pool[i]->tid==threadid)
		break;
	}
	while(1){
		if(thread_pool[i]->state == TERMINATED){
			if(__thread_return)
				*__thread_return = thread_pool[threadid]->retval;
			return 1;
		}
	}
	return 0;
}

int pthread_spin_init(pthread_spinlock *lock){
	// atomic_init(&lock->ticket,0);
	// atomic_init(&lock->turn,0);
	lock->ticket = 0;
	lock->turn = 0;
	return 0;
}

int pthread_spin_destroy(pthread_spinlock *lock){
	// pthread_spin_init(lock);
	return 0;
}

// Problem to think upon :-> Will multiple (different) locks by 'a' thread break this code  ???

int pthread_spin_lock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn);
	return 0;
}

int pthread_spin_unlock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->turn,1);

	return 0;
}

int pthread_mutex_init(pthread_mutex *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}

int pthread_mutex_lock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn){
		sched_yield();
	}
	return 0;
}

int pthread_mutex_unlock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	atomic_add(&lock->turn,1);

	return 0;
}




//ask sir about scheduler activation
// ask sir about signal handling in many-to-one
// ask sir about synchronisation
//PTHREAD_PROCESS_SHARED and PTHREAD_PROCESS_PRIVATE in spinlock_init?
//user needs to make sure that he/she declares the lock outside function


//pseudocode while loop inside mutex init till the lock becomes available, if not available, in M21, call scheduler andin 121 just continue while loop
