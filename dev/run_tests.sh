cc -Iinclude tests/test1.c combined.c
./a.out 0 >output.txt
str="126 output.txt"
if [[ $(wc -l output.txt) == "$str" ]]; then
    echo "basic test passed for one-one";
fi
./a.out 1 >output.txt
str="126 output.txt"
if [[ $(wc -l output.txt) == "$str" ]]; then
    echo "basic test passed for many-one";
fi


cc -Iinclude tests/mutex_trial.c combined.c
./a.out 0 >output.txt
if [[ $(diff <(head -n 1 output.txt) <(tail -n 1 output.txt)) == "" ]]; then
  echo "mutex test passed for one-one";
fi
./a.out 1 >output.txt
if [[ $(diff <(head -n 1 output.txt) <(tail -n 1 output.txt)) == "" ]]; then
  echo "mutex test passed for many-one";
fi

cc -Iinclude tests/spinlock_trial.c combined.c
./a.out 0 >output.txt
if [[ $(diff <(head -n 1 output.txt) <(tail -n 1 output.txt)) == "" ]]; then
  echo "spinlock test passed for one-one";
fi
./a.out 1 >output.txt
if [[ $(diff <(head -n 1 output.txt) <(tail -n 1 output.txt)) == "" ]]; then
  echo "spinlock test passed for many-one";
fi


rm a.out
rm output.txt
