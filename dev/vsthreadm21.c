#define _GNU_SOURCE
#include "vsthread.h"
#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)

void scheduler();

#define JB_SP 6
#define JB_PC 7
long int mangle(long int p) {
    long int ret;
    asm(" mov %1, %%rax;\n"
        " xor %%fs:0x30, %%rax;"
        " rol $0x11, %%rax;"
        " mov %%rax, %0;"
        : "=r"(ret)
        : "r"(p)
        : "%rax"
    );
    return ret;
}

unsigned long int atomic_add(volatile unsigned long int* p, unsigned long int incr){

    unsigned long int result;
    __asm__ __volatile__ ("lock; xadd %0, %1" :
            "=r"(result), "=m"(*p):
            "0"(incr), "m"(*p) :
            "memory");
			//clobers give extra information to compilers
			//here memory clobber tells compiler that we are going to access memory other than that mentioned in input/output operands - ie. accessing memory pointed by p
    return result;
}

void signal_handler(int signum){
	if(signum==SIGALRM){
		// printf("got alarm signal\n");
		scheduler();
	}
	else{
		printf("random signal\n");
	}
}

void pthread_execute (){
	thread_t *thread = thread_pool[fired_thread];
	execute_argument *args = (execute_argument *)thread->exe;
	if(setjmp(thread->exit_context)==0){
		args->function(args->arg);
	}
	ualarm(0,0);
	thread->state = TERMINATED;
	previous_thread = thread;
	scheduler();
}

void *createStack(int size){

	void *stack = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
		if (stack == MAP_FAILED)
			exit(2);

		return stack + size;
}


thread_t* get_running_process(){
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNING)
      return thread_pool[i];
  }
	// return previous_thread;
	return NULL;
}


thread_t* get_runnable_process(){
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNABLE)
      return thread_pool[i];
  }
	return NULL;
}

void freeup(thread_t *thread){
  return;
	int i = 0;
	printf("this -> %d %p\n",i++,thread->context);
	free(thread->context);
	printf("this -> %d %p\n",i++,thread->stack);
	munmap(thread->stack - STACK_SIZE,STACK_SIZE);
	printf("this -> %d %p\n",i++,thread);
	free(thread);
}

int cleanup(){
	int min = -1;
	for(int i =0; i< curr_thread_index;i++){
		if(thread_pool[i]->state == TERMINATED){
			freeup(thread_pool[i]);
			thread_pool[i]->state =EMBYRO;
			if(min == -1)
			min = i;
		}
		else if(thread_pool[i]->state == EMBYRO){
			if(min > i)
			min = i;
		}
	}
	return min;
}

int last_running_process(){
  // printf("z\n");
  int count = 0;
  for(int i=0;i<curr_thread_index;i++){
    if(thread_pool[i]->state==RUNNABLE || thread_pool[i]->state==RUNNING)
      count++;
  }
  if(count>=2){
    return 0;
  }
	return 1;
}

void scheduler(){
	ualarm(0,0);
	// printf("hi %d\n",curr_thread_index);
  thread_t *current_process = get_running_process();

  if(current_process==NULL){
    current_process = previous_thread;
  }
	if (current_process->state!=TERMINATED && current_process->state!=EMBYRO){
    // printf("making %d RUNNABLE\n",current_process->tid);
    // sleep(1);
		current_process->state = RUNNABLE;
	}
  //   printf("current_process = %p\ttid = %ld",current_process,current_process->tid);
  //   printf("hi2 %d\n",curr_thread_index);


	thread_t *next_thread;
	// printf("finding prc\n");
	int counter = 1;
	do{
		if((current_process->tid)+counter == curr_thread_index){
		  next_thread = thread_pool[0];
		}
		else{
			next_thread=thread_pool[(current_process->tid)+counter];
		}
		// printf("next_thread = %ld, status = %d\n",next_thread->tid,next_thread->state);
		counter+=1;
	}while(next_thread->state != RUNNABLE && next_thread->state != RUNNING);
	if(next_thread->tid==current_process->tid && previous_thread!=NULL){
		current_process=thread_pool[curr_thread_index-1];
	}
  	next_thread->state = RUNNING;
	signal(SIGALRM,signal_handler);
	// if(!last_running_process()){
	// 	ualarm(TIMER,0);
	// }
	// else{
	// 	printf("Last process\n");
	// }
  // printf("scheduler is calling %ld tid, swapping %ld tid\n",next_thread->tid,current_process->tid);

  previous_thread = NULL;
  if(sigsetjmp(current_process->context,1) == 2)
    return;

  	// printf("in sched\n");
	//   printf("PC = %ld\tSP = %ld\n",next_thread->context[0].__jmpbuf[JB_PC],next_thread->context[0].__jmpbuf[JB_SP]);
	//   printf("context = %p\n",next_thread->context);

  ualarm(TIMER,0);
  // if (swapcontext(current_process->context, next_thread->context) == -1)
  fired_thread = next_thread->tid;
  siglongjmp(next_thread->context,2);
  handle_error("swapcontext");
}



void pthread_init(){
	/* ----------- add main threads to the list of threads to be scheduled ------ */
	bitmap_contexts = (int*)malloc(sizeof(int)*64);
	for(int i=0;i<64;i++){
		bitmap_contexts[i] = 0;
	}
	curr_thread_index = 0;
	bitmap_contexts[0] = 1;
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	thread->state = RUNNING;
	thread->tid = curr_thread_index;
	// thread->context = (ucontext_t *)malloc(sizeof(ucontext_t));
	// getcontext(thread->context);
  // printf("here2\n");
  // thread->context = malloc(sizeof(jmp_buf));
//   jmp_buf temp;
  sigsetjmp(thread->context,1);
//   thread->context = &temp;
	thread_pool[curr_thread_index++] = thread;
	signal(SIGALRM,signal_handler);
	// ualarm(TIMER,0);
}

static void dummy(void){
	printf("I am test function\n");
}

extern int pthread_create(pthread_t *newthread, const pthread_attr_t *attr, void *(*start_routine)(void *), void *arg){
  	ualarm(0,0);
	thread_t *thread = (thread_t *)malloc(sizeof(thread_t));
	execute_argument *ex_thread = (execute_argument *)malloc(sizeof(execute_argument));
	ex_thread->arg = arg;
	ex_thread->function=start_routine;

	// printf("%p\n",ex_thread->function);
	thread->stack = createStack(STACK_SIZE);
	thread->exe = ex_thread;
	thread->state = RUNNABLE;
	int min;

	if ((min =cleanup()) != -1){
		thread->tid = min;
	}
	else{
		thread->tid = curr_thread_index;
	}

  sigsetjmp(thread->context,1);
	thread->context[0].__jmpbuf[JB_SP] = mangle((long int)thread->stack);
	thread->context[0].__jmpbuf[JB_PC] = mangle((long int)pthread_execute);
	// printf("PC = %ld\tSP = %ld\n",thread->context[0].__jmpbuf[JB_PC],thread->context[0].__jmpbuf[JB_SP]);
	// printf("context = %p\n",thread->context);
	thread_pool[min != -1 ? min : curr_thread_index++] = thread;
	*newthread = thread->tid;
  ualarm(TIMER,0);
	return 0;
}


void pthread_exit(void *retval){
	ualarm(0,0);
	thread_t *curr_thread=thread_pool[fired_thread];
	curr_thread->retval = retval;
	if(curr_thread->tid == 0)
		exit(0);
	longjmp(curr_thread->exit_context,1);

}


int pthread_join(pthread_t threadid, void **__thread_return){
	// printf("im here %p\n",&threadid);
	while(1){
		if(thread_pool[threadid]->state == TERMINATED){
      		// printf("returning to main\n");
			if(__thread_return)
				*__thread_return = thread_pool[threadid]->retval;
			return 0;
		}
	}
	return 0;
}

// Problem to think upon :-> Will multiple (different) locks by 'a' thread break this code  ???
int pthread_spin_init(pthread_spinlock *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}

int pthread_spin_destroy(pthread_spinlock *lock){
	// pthread_spin_init(lock);
	return 0;
}


int pthread_spin_lock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	while (lock->turn != thread->myturn);
	return 0;
}

int pthread_spin_unlock(pthread_spinlock *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->turn,1);

	return 0;
}


int pthread_mutex_init(pthread_mutex *lock){
	atomic_init(&lock->ticket,0);
	atomic_init(&lock->turn,0);
	return 0;
}

int pthread_mutex_lock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	thread->myturn = atomic_add(&lock->ticket,1);
	// if(thread->tid == 2)
	// printf("Incrementing ticket for %ld process\n",thread->tid);
	while (lock->turn != thread->myturn){
		// printf("i didn't get the lock :( %ld %ld %ld %ld\n",thread->tid,thread->myturn,lock->ticket,lock->turn);
		scheduler();
	}
	return 0;
}

int pthread_mutex_unlock(pthread_mutex *lock){
	thread_t *thread;
	if( (thread = get_running_process()) == NULL)
		return 1;
	atomic_add(&lock->turn,1);
		// if(thread->tid == 2)
	// printf("releasing ticket for %ld process\n",thread->tid);

	// lock->turn += 1;

	return 0;
}
//IMPORTANT

/*
  * first time it takes a lot of time in swapping thread 1 with 2 (does that after TIMER) because it is the only thread acquiring locks and can acquire all tickets !
  * but then since thread 2 also gets scheduled and its locks are also pending,
  * so when thread 1 is scheduled the 2nd time, it also calls scheduler since it cannot acquire locks due to pending locks of 2
  */



// int pthread_spin_init(pthread_spinlock_t *lock, int pshared){
// 	if(!spinlocks){
// 		num_destroyed_locks = 0;
// 		spinlocks = (pthread_spinlock *)malloc(sizeof(pthread_spinlock)*MAX_SPIN_LOCKS);
// 	}
// 	int min_free_lock = -1;
// 	if(num_destroyed_locks){
// 		for(int i = 0; i < curr_spinlock_index;i++){
// 			if(spinlocks[i].lock==NULL){
// 				min_free_lock = i;
// 				num_destroyed_locks--;
// 				break;
// 			}
// 		}
// 	}
// 	else if(curr_spinlock_index && curr_spinlock_index % MAX_SPIN_LOCKS == 0){
// 		spinlocks = realloc(spinlocks,curr_spinlock_index+MAX_SPIN_LOCKS);
// 		min_free_lock = curr_spinlock_index++;
// 	}
// 	else{
// 		min_free_lock = curr_spinlock_index++;
// 	}
// 	spinlocks[min_free_lock].lock = lock;
// 	spinlocks[min_free_lock].thread=  NULL;
// 	*lock = min_free_lock;
// 	return 0;

// }

// int pthread_spin_destroy(pthread_spinlock_t *lock){
// 	spinlocks[*lock].lock = NULL;
// 	num_destroyed_locks++;
// 	return 0;
// }

// int pthread_spin_lock(pthread_spinlock_t *lock){
// 	ualarm(0,0);
// 	if(spinlocks[*lock].thread ==  NULL){
// 		spinlocks[*lock].thread = get_running_process();
// 		alarm(5);
// 		return 0;
// 	}
// 	else{
// 		while(1){
// 			//check if one-one, then don't call scheduler
// 			raise(SIGALRM);
// 			if(spinlocks[*lock].thread==NULL){
// 				spinlocks[*lock].thread = get_running_process();
// 				alarm(5);
// 				return 0;
// 			}

// 		}
// 	}

// }

// int pthread_spin_unlock(pthread_spinlock_t *lock){
// 	ualarm(0,0);
// 	spinlocks[*lock].thread = NULL;
// 	alarm(5);
// 	return 0;
// }
