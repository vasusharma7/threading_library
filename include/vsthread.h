#define _GNU_SOURCE
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <sched.h>
#include <setjmp.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <ucontext.h>
#include <stdatomic.h>
#include<unistd.h>
#define STACK_SIZE (1024*1024*4)
#define MAX_THREADS 64
#define MAX_SPIN_LOCKS 128
#define TIMER 200
typedef unsigned long int pthread_t;

void *createStack(int size);

extern int pthread_create(pthread_t *newthread, const pthread_attr_t *attr,
                          void *(*start_routine)(void *), void *arg);

extern int pthread_join(pthread_t threadid, void **__thread_return);
extern int *pthead_execute(void *(*start_routine), void *arg);
extern void pthread_exit(void *retval);
extern pthread_t pthread_self(void);

extern int pthread_init();
void scheduler();

enum thread_state { EMBYRO, RUNNABLE, RUNNING, TERMINATED, SLEEP};
enum thread_type {MANY_ONE, ONE_ONE, UNDECIDED};
enum thread_type GLOBAL_MODE;
typedef struct execute_argument{
 void * (*function)(void *);
 void * arg;
 void *thread;
} execute_argument;

typedef struct thread_t{
 void *stack;
//  ucontext_t *context;
 jmp_buf context;
 jmp_buf exit_context;
 pthread_t tid;
 pid_t ppid;
 void *exitStatus;
 enum thread_state state;
 execute_argument *exe;
 unsigned long int myturn;
 void *retval;
} thread_t;

int pthread_kill(pthread_t threadid, int signo);

typedef struct pthread_spinlock {
  // atomic_ulong ticket;
  unsigned long int ticket;
  // atomic_ulong turn;
  unsigned long int turn;
  // thread_t *thread;
} pthread_spinlock;

typedef struct pthread_mutex {
  unsigned long int ticket;
  unsigned long int turn;
} pthread_mutex;

int pthread_spin_lock(pthread_spinlock *lock);
int pthread_spin_unlock(pthread_spinlock *lock);
int pthread_spin_init(pthread_spinlock *lock);
int pthread_spin_destroy(pthread_spinlock *lock);

int pthread_mutex_lock(pthread_mutex *lock);
int pthread_mutex_unlock(pthread_mutex *lock);
int pthread_mutex_init(pthread_mutex *lock);
int pthread_mutex_destroy(pthread_mutex *lock);

thread_t *previous_thread;
int curr_thread_index;
thread_t *thread_pool[MAX_THREADS];
int curr_spinlock_index;
int *bitmap_contexts;
int num_destroyed_locks;
int fired_thread;
